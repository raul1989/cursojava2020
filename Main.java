import src.java.polymorphism.Animal;
import src.java.polymorphism.Cocodrile;
import src.java.polymorphism.Duck;
import src.java.polymorphism.Flyer;
import src.java.polymorphism.Swimmer;

public class Main {

    public static void main(String[] args) {
        Animal donald = new Duck();
        donald.setName("Donald");
        donald.eat();

        // Animal[] farm = new Animal[2];
        // farm[0] = new Duck();
        // farm[0].setName("Lucas");
        // farm[1] = new Cocodrile();
        // farm[1].setName("Costenia");

        // for(Animal animal : farm) {
        //     animal.eat();
        //     if (animal instanceof Duck) {
        //         ((Duck)animal).quak();
        //     }
        // }

        Flyer lucas = new Duck();
        Swimmer swimmer1 = ((Swimmer)lucas);
        lucas.fly();
        ((Duck)lucas).quak();
        swimmer1.swim();

    }
}