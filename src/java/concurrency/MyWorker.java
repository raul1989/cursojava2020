package src.java.concurrency;

public class MyWorker extends Thread {
    private String id;

    //Constructor
    public MyWorker(String name) {
        id = name;
    }

    public String toString() {
        return "My name is: " + id;
    }

    public void run() {
        for(int i=0; i<10; i++) {
            System.out.println(id + " is working...");
        }
    }
}