package src.java.polymorphism;

public abstract class Animal {
    protected String name;

    public void setName(String name) {
        this.name=name;
    }

    public String getName() {return name;}

    public abstract void born();

    public void eat() {
        System.out.println(name + " is eating...");
    }

    public void sleep() {
        System.out.println("It is sleeping...");
    }

    public void poop() {
        System.out.println("It is pooping...");
    }

}