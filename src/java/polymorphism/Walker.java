package src.java.polymorphism;

public interface Walker {
    public void walk();
}