package src.java.polymorphism;

public class Duck extends Animal implements Swimmer, Walker, Flyer {

    public void swim() {
        System.out.println(name + " is swimming...");
    }

    public void walk() {
        System.out.println(name + " is walking...");
    }

    public void fly() {
        System.out.println(name + " is flying...");
    }

    public void born() {
        System.out.println(name + " is from an egg...");
    }

    public void quak() {
        System.out.println(name + " doing quak...");    
    }
}