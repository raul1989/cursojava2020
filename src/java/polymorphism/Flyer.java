package src.java.polymorphism;

public interface Flyer {
    public void fly();
}