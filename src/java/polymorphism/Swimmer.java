package src.java.polymorphism;

public interface Swimmer {
    public void swim();
}