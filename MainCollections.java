import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import src.java.collections.A3;
import src.java.collections.Auto;
import src.java.collections.Spark;

public class MainCollections {
    public static void main(String[] args) {
        List<Auto> autos1 = new ArrayList<Auto>();         
        List<Auto> autos2 = new LinkedList<Auto>();

        A3 a3 = new A3();
        Spark spark = new Spark();
        autos1.add(a3);
        autos1.add(spark);
        autos2.add(a3);
        autos2.add(spark);
        
        for(Auto auto : autos1) {
            auto.push();
        }

        for(Auto auto : autos2) {
            auto.startEngine();
        }

        System.out.println("*********************map**********************");

        Map<String, Auto> map1 = new HashMap<String, Auto>();
        Map<String, Auto> map2 = new TreeMap<String, Auto>();
        Map<String, List<String>> phoneDirectory = new HashMap<String, List<String>>();
        phoneDirectory.put("antonio", Arrays.asList("3331967422", "3312345678"));

        map1.put("raul", a3);
        map1.put("antonio", spark);
        map2.put("raul", a3);
        map2.put("antonio", spark);

        System.out.println(map1.get("raul"));


    }

}