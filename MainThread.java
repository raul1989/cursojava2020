import src.java.concurrency.MyWorker;
import src.java.concurrency.Task;

public class MainThread {

    public static void main(String[] args) {
        MyWorker w1 = new MyWorker("Thread 1");
        MyWorker w2 = new MyWorker("Thread 2");
        MyWorker w3 = new MyWorker("Thread 3");

        Task task = new Task();
        Thread t1 = new Thread(task);
        t1.start();

        w1.start();
        w2.start();
        w3.start();
    }
}