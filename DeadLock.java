
public class DeadLock {
    static String r1 = "resource1";
    static String r2 = "resource2";
    public static void main(String[] args) {

        new Thread() {
            public void run() {
                while(true)
                synchronized(r1) {
                    System.out.println("t1 is using " + r1);
                    synchronized(r2) {
                        System.out.println("t1 is using " + r2);
                    }
                }
            }
        }.start();

        new Thread() {
            public void run() {
                while(true)
                synchronized(r2) {
                    System.out.println("t2 is using " + r2);
                    synchronized(r1) {
                        System.out.println("t2 is using " + r1);                      
                    }
                }
            }
        }.start();
    }
}